package ninjacat.game.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ninjacat.game.NinjaCat;
import ninjacat.game.util.Audio;

public class GameOverScreen extends AbstractScreen{

    NinjaCat ninjaCat;

    SpriteBatch batch = new SpriteBatch();

    Texture win;
    Texture lose;
    Texture img;

    public GameOverScreen(final NinjaCat ninjaCat, boolean end){
        this.ninjaCat = ninjaCat;

        win = new Texture("images\\win.png");
        lose = new Texture("images\\lose.png");
        endChoice(end);
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode){
                    case Input.Keys.ENTER:
                        Audio.stopWin();
                        Audio.stopLose();
                        ninjaCat.setMenuScreen();
                        break;
                }
                return true;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                return true;
            }
        });
    }

    public void endChoice(boolean end){
        if(end){
            img = win;
        } else{
            img = lose;
        }
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();
        batch.draw(img, 0, 0, GameScreen.WIDTH, GameScreen.HEIGHT);
        batch.end();
    }
}