package ninjacat.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class AbstractScreen implements Screen {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    protected Stage stage;
    public Camera camera;
     public Viewport viewport;

    protected AbstractScreen(){
        this.stage = new Stage();
        camera = new OrthographicCamera();
        viewport = new ScalingViewport(Scaling.fit, WIDTH, HEIGHT, camera);
        camera.translate(WIDTH/2, HEIGHT/2, 0);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage.setViewport(viewport);
    }

    @Override
    public void render(float delta) {
        stage.getCamera().update();
        stage.act(delta);
//        Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
//        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
//        Gdx.gl.glEnable(GL20.GL_BLEND);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor( stage );
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
