package ninjacat.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import ninjacat.game.NinjaCat;
import ninjacat.game.models.Level;
import ninjacat.game.util.Audio;

public class GameScreen extends AbstractScreen{

    public Level level;
    public NinjaCat ninjaCat;

    public GameScreen(final NinjaCat ninjaCat){
        this.ninjaCat = ninjaCat;
        level = new Level(this);
        stage.addActor(level);
        newCamera();
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                level.keyDown(keycode);
                switch (keycode){
                    case Input.Keys.ESCAPE:
                        Audio.stopGame();
                        ninjaCat.setMenuScreen();
                        break;
                }
                return true;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                level.keyUp(keycode);
                return true;
            }
        });
    }

    public void newCamera(){
        camera = new OrthographicCamera();
        viewport = new ScalingViewport(Scaling.fit, WIDTH, HEIGHT, camera);
        camera.translate(WIDTH/2, HEIGHT/2, 0);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage.setViewport(viewport);
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }
}