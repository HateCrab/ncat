package ninjacat.game.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ninjacat.game.NinjaCat;
import ninjacat.game.util.Audio;
import ninjacat.game.util.Button;

import java.util.ArrayList;

public class MenuScreen extends AbstractScreen{

    ArrayList<Button>buttons = new ArrayList<Button>();
    Button tmp;

    NinjaCat ninjaCat;

    SpriteBatch batch = new SpriteBatch();

    Texture img;

    public MenuScreen(final NinjaCat ninjaCat){
        this.ninjaCat = ninjaCat;

        img = new Texture("images\\menu.png");

        for(int i= 1; i < 4; i++){
            buttons.add(new Button(i, ninjaCat));
        }

        for (Button button : buttons) {
            stage.addActor(button);
        }

        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode){
                    case Input.Keys.ENTER:
                        Audio.stopMenu();
                        ninjaCat.setGameScreen();
                        break;
                }
                return true;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Object tmp = event.getTarget();
                if(tmp instanceof Button) {
                    switch (((Button) tmp).stat) {
                        case Button.START:
                            Audio.stopMenu();
                            ninjaCat.setGameScreen();
                            break;
                        case Button.HELP:
                            ninjaCat.setHelpScreen();
                            break;
                        case Button.EXIT:
                            System.exit(-1);
                            break;
                        case Button.BACK:
                            ninjaCat.setMenuScreen();
                            break;
                    }
                }

            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                return true;
            }
        });
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        batch.begin();
        batch.draw(img, 0, 0, GameScreen.WIDTH, GameScreen.HEIGHT);
        batch.end();
        super.render(delta);

    }
}
