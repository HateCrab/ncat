package ninjacat.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;

import ninjacat.game.NinjaCat;

public class Audio{

    static final String DEAD = "dead";
    static final String BOOM = "boom";
    static final String BONUS = "bonus";
    static final String WIN = "win";
    static final String LOSE = "lose";
    static final String MENU = "menu";
    static final String SVIST = "svist";
    static final String GAME = "game";
    static final String HLOP = "hlop";
    static final String SWORD = "sword";


    static HashMap<String, Sound> sounds;
    static HashMap<String, Music> game;
    static HashMap<String, Music> menu;
    static HashMap<String, Music> win;
    static HashMap<String, Music> lose;



    public static void playDead() {
        if (NinjaCat.sound && sounds != null) {
            sounds.get(DEAD).play();
        }
    }

    public static void playHlop() {
        if (NinjaCat.sound && sounds != null) {
            sounds.get(HLOP).play();
        }
    }

    public static void playBoom() {
        if (NinjaCat.sound && sounds != null) {
            sounds.get(BOOM).play();
        }
    }
    public static void playBonus() {
        if (NinjaCat.sound && sounds != null) {
            sounds.get(BONUS).play();
        }
    }

    public static void playSvist() {
        if (NinjaCat.sound && sounds != null) {
            sounds.get(SVIST).play();
        }
    }

    public static void playSword() {
        if (NinjaCat.sound && sounds != null) {
            sounds.get(SWORD).play();
        }
    }

    public static void playGame() {
        if (NinjaCat.sound && game != null && !game.get(GAME).isPlaying()) {
            game.get(GAME).play();
        }
    }

    public static void playMenu() {
        if (NinjaCat.sound && menu != null && !menu.get(MENU).isPlaying()) {
            menu.get(MENU).play();
        }
    }

    public static void playWin() {
        if (NinjaCat.sound && win != null && !win.get(WIN).isPlaying()) {
            win.get(WIN).play();
        }
    }

    public static void playLose() {
        if (NinjaCat.sound && lose != null && !lose.get(LOSE).isPlaying()) {
            lose.get(LOSE).play();
        }
    }

    public static void stopWin() {
        if (NinjaCat.sound && win != null && win.get(WIN).isPlaying()) {
            win.get(WIN).stop();
        }
    }

    public static void stopLose() {
        if (NinjaCat.sound && lose != null && lose.get(LOSE).isPlaying()) {
            lose.get(LOSE).stop();
        }
    }

    public static void stopGame() {
        if (NinjaCat.sound && game != null && game.get(GAME).isPlaying()) {
            game.get(GAME).stop();
        }
    }

    public static void stopMenu() {
        if (NinjaCat.sound && menu != null && menu.get(MENU).isPlaying()) {
            menu.get(MENU).stop();
        }
    }

    public static void dispose() {
        for (Sound sound : sounds.values()) {
            sound.dispose();
        }
        sounds.clear();
    }

    public static void load() {
        if (sounds == null) {
            sounds = new HashMap<String, Sound>();
        }
        if (game == null){
            game = new HashMap<String, Music>();
        }
        if (menu == null){
            menu = new HashMap<String, Music>();
        }
        if (win == null){
            win = new HashMap<String, Music>();
        }
        if (lose == null){
            lose = new HashMap<String, Music>();
        }
        game.put(GAME, Gdx.audio.newMusic(Gdx.files.internal("sounds\\game.mp3")));
        menu.put(MENU, Gdx.audio.newMusic(Gdx.files.internal("sounds\\menu.mp3")));
        win.put(WIN, Gdx.audio.newMusic(Gdx.files.internal("sounds\\win.mp3")));
        lose.put(LOSE, Gdx.audio.newMusic(Gdx.files.internal("sounds\\lose.mp3")));
        load(DEAD);
        load(BONUS);
        load(BOOM);
        load(SWORD);
        load(HLOP);
        load(SVIST);
    }

    private static void load(String name) {
        Sound sound = sounds.get(name);
        if (sound != null)
            sound.dispose();
        sounds.put(name, Gdx.audio.newSound(Gdx.files.internal("sounds/" + name+".wav")));
    }


}