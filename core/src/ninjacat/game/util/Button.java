package ninjacat.game.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ninjacat.game.NinjaCat;

import java.awt.event.InputEvent;

public class Button extends Actor{

    public static final int START = 1;
    public static final int HELP = 2;
    public static final int EXIT = 3;
    public static final int BACK = 4;

    public int stat;

    Texture img;

    NinjaCat ninjaCat;

    public Button(int num, final NinjaCat ninjaCat){
        switch (num){
            case START:
                stat = START;
                img = new Texture("images\\start.png");
                setBounds(110, 140, 162, 50);
                break;
            case HELP:
                stat = HELP;
                img = new Texture("images\\help.png");
                setBounds(110, 80, 120, 50);
                break;
            case EXIT:
                stat = EXIT;
                img = new Texture("images\\exit.png");
                setBounds(110, 20, 115, 50);
                break;
            case BACK:
                stat = BACK;
                img = new Texture("images\\back.png");
                setBounds(620, 20, 143, 72);
                break;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(img, getX(), getY());
    }
}
