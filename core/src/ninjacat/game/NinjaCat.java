package ninjacat.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import ninjacat.game.screens.*;
import ninjacat.game.util.Audio;

public class NinjaCat extends Game {

    public static boolean sound;
    GameScreen gameScreen;
    GameOverScreen gameOverScreen;
    MenuScreen menuScreen;
    HelpScreen helpScreen;

    @Override
    public void create () {
        Audio.load();
        sound = true;
        gameScreen = new GameScreen(this);
        menuScreen = new MenuScreen(this);
        helpScreen = new HelpScreen(this);
        setMenuScreen();
    }

    public void setGameScreen(){
        Audio.playGame();
        if((gameScreen.level.level == 4 && !gameScreen.level.boss.alive) || (gameScreen.level.player.lifeNum < 1)){
            gameScreen.dispose();
            gameScreen = new GameScreen(this);
        }
        setScreen(gameScreen);
    }

    public void setGameOverScreen(boolean end){
        if(end){
            Audio.playWin();
        } else {
            Audio.playLose();
        }
        gameOverScreen = new GameOverScreen(this, end);
        setScreen(gameOverScreen);
    }

    public void setMenuScreen(){
        Audio.playMenu();
        setScreen(menuScreen);
    }

    public void setBoss(){
        gameScreen.level.level = 4;
        gameScreen.level.setLevel(4);
        setGameScreen();
    }

    public void setHelpScreen(){
        setScreen(helpScreen);
    }
}