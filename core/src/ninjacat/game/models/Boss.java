package ninjacat.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ninjacat.game.util.Audio;

import java.util.Random;

public class Boss extends Actor {

    public static final float AGRO_TIME = 1f;
    public static final float SIT_TIME = 4f;

    Texture img;
    Texture bossSit;
    Texture bossAgro;
    Texture h1;
    Texture h2;
    Texture h3;
    Texture health;

    boolean agro;
    boolean sit;
    public boolean alive;

    int stage;
    int sitPoz;
    int healthNum;

    float step;
    float sitTimer = 0f;
    float agroTimer = 0f;

    Random random = new Random();

    Rectangle body = new Rectangle();

    public Boss(){
        step = 200;
        healthNum = 3;
        alive = true;
        agro = true;
        sit = false;
        setBounds(1430, 100, 125, 200);
        body = new Rectangle(getX(), getY(), getWidth(), getHeight());
        bossSit = new Texture("images\\boss_sit.png");
        bossAgro = new Texture("images\\boss_agro.png");
        h1 = new Texture("images\\h1.png");
        h2 = new Texture("images\\h2.png");
        h3 = new Texture("images\\h3.png");
        img = bossAgro;
        health = h3;
    }

    public void setRect(){
        body.set(getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        sit(delta);
        agro(delta);
    }

    public void agro(float delta){
        if(agro) {
            if(stage < 9) {
                if (agroTimer < AGRO_TIME) {
                    agroTimer += delta;
                } else {
                    sitPoz = 1 + random.nextInt(8);
                    stage += 1;
                    agroTimer = 0;
                    poz(sitPoz);
                }
            }
            if(stage == 9){
                    agro = false;
                    sit = true;
                    sitTimer = 0;
                    img = bossSit;
                }

        }
    }

    public void poz(int sitPoz){
        switch (sitPoz){
            case 1:
                setY(100);
                setX(630);
                setRect();
                break;
            case 2:
                setY(100);
                setX(830);
                setRect();
                break;
            case 3:
                setY(100);
                setX(1030);
                setRect();
                break;
            case 4:
                setY(100);
                setX(1230);
                setRect();
                break;
            case 5:
                setY(100);
                setX(1430);
                setRect();
                break;
            case 6:
                setX(230);
                setY(340);
                setRect();
                break;
            case 7:
                setX(565);
                setY(340);
                setRect();
                break;
            case 8:
                setX(1385);
                setY(340);
                setRect();
                break;
        }
    }

    public void sit(float delta){
        if(sit){
            if(sitTimer == 0) {
                Audio.playHlop();
                stage = 1;
                sitPoz = 1 + random.nextInt(3);
                switch (sitPoz) {
                    case 1:
                        setX(410);
                        setY(600);
                        setRect();
                        break;
                    case 2:
                        setX(910);
                        setY(465);
                        setRect();
                        break;
                    case 3:
                        setX(1680);
                        setY(410);
                        setRect();
                        break;
                }
            }
            if (sitTimer < SIT_TIME){
                sitTimer += delta;
            } else {
                sit = false;
                agro = true;
                setX(1430);
                setY(100);
                setRect();
                agroTimer = 0;
                img = bossAgro;
            }
        }
    }

    public void spawn(){
        agroTimer = 0;
        stage = 1;
        agro = true;
        sit = false;
        setX(1430);
        setY(100);
        setRect();
        img = bossAgro;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(img, getX(), getY());
    }
}
