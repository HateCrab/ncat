package ninjacat.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ninjacat.game.util.Audio;

public class Pink extends Actor {

    public static final float BOOM_TIME = 0.5f;

    float boomTimer = 0;

    Texture img;

    Rectangle body;

    boolean alive;
    boolean dead;

    public Pink(float x){
        alive = true;
        dead = false;
        setBounds(x, 60, 114, 161);
        body = new Rectangle(getX(), getY(), getWidth(), getHeight());
        img = new Texture("images\\pink.png");
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        boom(delta);
    }

    public void boom(float delta){
        if(!alive && boomTimer == 0) {
            Audio.playBoom();
            setX(getX() - 40);
        }
        if(!alive && boomTimer < BOOM_TIME){

            img = new Texture("images\\boom.png");
            boomTimer += delta;
        } else{
            if(boomTimer >= BOOM_TIME){
                dead = true;
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(img, getX(), getY());
    }
}
