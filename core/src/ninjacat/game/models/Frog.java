package ninjacat.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Frog extends Actor{

    public static final float JUMP_WIDTH = 145;
    public static final float DOWN_WIDTH = 132;
    public static final float SIT_WIDTH = 107;
    final float JUMP_TIME = 0.4f;
    final float DOWN_TIME = 0.4f;
    final float SIT_TIME = 2f;

    Texture img;
    Texture jumpT;
    Texture sitT;
    Texture downT;

    float sitTimer = 0;
    float downTimer = 0;
    float jumpTimer = 0;

    boolean jump;
    boolean sit;
    boolean down;
    boolean alive;

    Rectangle body;

    public Frog(float x){
        alive = true;
        down = false;
        sit = false;
        jump = true;
        setBounds(x, 100, SIT_WIDTH, 101);
        sitT = new Texture("images\\frog1.png");
        jumpT = new Texture("images\\frog2.png");
        downT = new Texture("images\\frog3.png");
        img = sitT;
        body = new Rectangle(getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        sit(delta);
        jump(delta);
        down(delta);
    }

    public void sit(float delta){
        if(sit ) {
            img = sitT;
            if (sitTimer < SIT_TIME) {
                sitTimer += delta;
            } else {
                sit = false;
                jump = true;
                jumpTimer = 0;
                setX(getX() - getHeight());
                setWidth(JUMP_WIDTH);
                setRect();
            }
        }
    }

    public void jump(float delta){
        if(jump) {
            img = jumpT;
            if (jumpTimer < JUMP_TIME / 2) {
                jumpTimer += delta;
            } else {
                jump = false;
                down = true;
                downTimer = 0;
                setX(getX() - 80);
                setWidth(DOWN_WIDTH);
                setRect();
            }
        }
    }

    public void down(float delta){
        if(down) {
            img = downT;
            if (downTimer < DOWN_TIME) {
                downTimer += delta;
            } else {
                down = false;
                sit = true;
                sitTimer = 0;
                setWidth(SIT_WIDTH);
                setRect();
            }
        }
    }

    public void setRect(){
        body.set(getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(img, getX(), getY());
    }
}
