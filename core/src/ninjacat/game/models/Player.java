package ninjacat.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ninjacat.game.util.Audio;

public class Player extends Actor {

    public static final float PLAYER_WIDTH = 71;
    public static final float PLAYER_HEIGHT = 156;
    public static final float SWORD_WIDTH = 104;
    public static final float SWORD_HEIGHT = 35;
    public static final float SHURIKEN_WIDTH = 35;
    public static final float SHURIKEN_HEIGHT = 35;
    final float SHUR_TIME = 1f;
    final float STEP_TIME = 0.2f;
    final float JUMP_TIME = 0.5f;
    final float HIT_TIME = 0.2f;

    float stepTimer = 0;
    float shurTimer = 0;
    float jumpTimer = 0;
    float mg = 0;
    float speed = 12;
    float hitTimer = 0;

    Rectangle body;
    Rectangle sword;
    Rectangle shuriken;
    public boolean jump;
    public boolean onPlatform;
    public boolean fallen;
    public boolean hit;
    public boolean shurR;
    public boolean shurL;

    public int shurNum;
    public int lifeNum;

    boolean direction;

    public boolean[] acts = new boolean[5];
    TextureAtlas atlas;
    TextureRegion img;
    Texture swordTexture;
    Texture swordRight;
    Texture swordLeft;
    Texture shurikenTexture;

    public Player(){
        lifeNum = 3;
        shurNum = 5;
        fallen = true;
        onPlatform = false;
        hit = false;
        direction = true;
        setBounds(100, 100, PLAYER_WIDTH, PLAYER_HEIGHT);

        body = new Rectangle(getX(), getY(), PLAYER_WIDTH, PLAYER_HEIGHT);
        sword = new Rectangle(getX()+45, getY()+50, SWORD_WIDTH, SWORD_HEIGHT);
        shuriken = new Rectangle(getX()+45, getY()+50, SHURIKEN_WIDTH, SHURIKEN_HEIGHT);

        atlas = new TextureAtlas("Player.atlas");
        img = atlas.findRegion("stop_right");

        swordRight = new Texture("images\\hand-forward.png");
        swordLeft = new Texture("images\\hand-back.png");
        swordTexture = swordRight;

        shurikenTexture = new Texture("images\\shur.png");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if(hit){
            batch.draw(swordTexture, sword.getX(), sword.getY());
        }
        if(shurR || shurL){
            batch.draw(shurikenTexture, shuriken.getX(), shuriken.getY());
        }
        batch.draw(img, getX(), getY());
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(acts[0]){
            moveRight(delta);
        }
        if(acts[1]){
            if(!acts[0]) {
                moveLeft(delta);
            }
        }
        if (!acts[0] && !acts[1]){
            if(direction){
                img = atlas.findRegion("stop_right");
            } else {
                img = atlas.findRegion("stop_left");
            }
        }
        if(acts[2]){
            if(!jump && fallen) {
                speed = 35;
                jumpTimer = 0;
                jump = true;
            }
        }
        if(acts[3]){
            if(!hit) {
                Audio.playSword();
                hitTimer = 0;
                hit = true;
            }
            acts[3] = false;
        }
        if(acts[4]){
            if(!shurR && !shurL && shurNum > 0) {
                Audio.playSvist();
                shurTimer = 0;
                shurNum -= 1;
                if (direction) {
                    shurR = true;
                    shuriken.set(getX() + 45, getY() + 50, SHURIKEN_WIDTH, SHURIKEN_HEIGHT);
                } else {
                    shurL = true;
                    shuriken.set(getX(), getY() + 50, SHURIKEN_WIDTH, SHURIKEN_HEIGHT);
                }
            }
        }

        //удар
        if(hit){
            hit(delta);
        }

        //прыжок
        if(jump) {
            jump(delta);
        }
        fall();

        if(shurR){
            castRight(delta);
        }
        if(shurL){
            castLeft(delta);
        }
    }

    public void moveRight(float delta){
        direction = true;
        if(stepTimer < STEP_TIME /2 && !jump && fallen){
            img = atlas.findRegion("stop_right");
            stepTimer += delta;
        } else{
            if(stepTimer < STEP_TIME && !jump && fallen){
                img = atlas.findRegion("jump_right");
                stepTimer += delta;
            } else {
                stepTimer = 0;
            }
        }
        setX(getX()+5);
        setRect();
    }

    public void moveLeft(float delta){
        direction = false;
        if(stepTimer < STEP_TIME /2 && !jump && fallen){
            img = atlas.findRegion("stop_left");
            stepTimer += delta;
        } else{
            if(stepTimer < STEP_TIME && !jump && fallen){
                img = atlas.findRegion("jump_left");
                stepTimer += delta;
            } else {
                stepTimer = 0;
            }
        }
        setX(getX()-5);
        setRect();
    }

    public void jump(float delta){
        if (jumpTimer < JUMP_TIME && speed > 0) {
            if(direction){
                img = atlas.findRegion("jump_right");
            }else {
                img = atlas.findRegion("jump_left");
            }
            jumpTimer += delta;
            speed -= 2;
            setY(getY() + speed);
            setRect();
        } else {
            jump = false;
            mg = 0;
        }
    }

    public void fall(){
        if(getY() > Level.FLOOR && !onPlatform){
            fallen = false;
        }
        if(!fallen && !jump && !onPlatform){
            if(direction){
                img = atlas.findRegion("jump_right");
            }else {
                img = atlas.findRegion("jump_left");
            }
            mg += 2;
            setY(getY() - mg);
            setRect();
        }
        if(getY() < Level.FLOOR){
            setY(Level.FLOOR);
        }
        if(getY() == Level.FLOOR || onPlatform){
            fallen = true;
        }
        if(fallen){
            mg = 0;
        }
    }

    public void hit(float delta){
        if(direction) {
            swordTexture = swordRight;
            if (hitTimer < HIT_TIME) {
                sword.set(getX() + 45, getY() + 50, SWORD_WIDTH, SWORD_HEIGHT);
                hitTimer += delta;
            } else {
                hit = false;
            }
        } else{
            swordTexture = swordLeft;
            if (hitTimer < HIT_TIME) {
                sword.set((getX() + 25) - SWORD_WIDTH, getY() + 50, SWORD_WIDTH, SWORD_HEIGHT);
                hitTimer += delta;
            } else {
                hit = false;
            }
        }
    }

    public void castRight(float delta){
        if (shurTimer < SHUR_TIME) {
            shuriken.setX(shuriken.getX() + 10);
            shurTimer += delta;
        } else {
            shurR = false;
        }
    }

    public void castLeft(float delta){
        if (shurTimer < SHUR_TIME) {
            shuriken.setX(shuriken.getX() - 10);
            shurTimer += delta;
        } else {
            shurL = false;
        }
    }

    public void reSpawn(){
        setX(100);
        setY(100);
        setRect();
    }

    public void setRect(){
        body.set(getX(), getY(), getWidth(), getHeight());
    }
}
