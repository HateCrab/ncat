package ninjacat.game.models;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import ninjacat.game.screens.GameScreen;
import ninjacat.game.util.Audio;

import java.util.ArrayList;

public class Level extends Group {

    public static float FLOOR = 100;
    public static float LEFT = GameScreen.WIDTH/8;
    public static float RIGHT = GameScreen.WIDTH - 400;
    public static float UP = GameScreen.HEIGHT - GameScreen.HEIGHT/4;
    public static float DOWN = GameScreen.HEIGHT/2;
    public static float LIFE_DRAW = 50;
    public static float SHUR_DRAW = 700;

    ArrayList<Platform>platforms = new ArrayList<Platform>();
    ArrayList<Frog>frogs = new ArrayList<Frog>();
    ArrayList<Spider>spiders = new ArrayList<Spider>();
    ArrayList<Pink>pinks = new ArrayList<Pink>();
    ArrayList<Bonus>pizzas = new ArrayList<Bonus>();
    ArrayList<Bonus>shurikens = new ArrayList<Bonus>();
    ArrayList<Bonus>lives = new ArrayList<Bonus>();
    ArrayList<Object> delete = new ArrayList<Object>();

    GameScreen gameScreen;

    float screenX=0;
    float screenY=0;
    float leftLine = 0;
    float rightLine = 12500;
    float downLine = 300;

    public Boss boss;
    public Player player;

    public int level;

    Texture img;
    Texture img2;
    Texture imgLife;
    Texture imgShur;

    public Level(GameScreen gameScreen){
        img = new Texture("images\\level1_1.png");
        img2 = new Texture("images\\level1_2.png");
        imgLife = new Texture("images\\life.png");
        imgShur = new Texture("images\\shuriken.png");
        this.gameScreen = gameScreen;
        level = 1;
        player = new Player();
        addActor(player);
        setLevel(level);
    }

    //создание левла
    public void setLevel(int level) {
        switch (level) {
            case 1:
                deleteLevel();

                rightLine = 12500;
                downLine = 300;

                img = new Texture("images\\level1_1.png");
                img2 = new Texture("images\\level1_2.png");
                //level 1
                platforms.add(new Platform(350, 300, 200, 30));
                platforms.add(new Platform(660, 270, 100, 30));
                platforms.add(new Platform(870, 270, 100, 30));
                platforms.add(new Platform(1080, 270, 100, 30));
                platforms.add(new Platform(1310, 270, 100, 30));
                platforms.add(new Platform(1550, 270, 100, 30));
                platforms.add(new Platform(1795, 270, 100, 30));
                platforms.add(new Platform(2130, 270, 100, 30));
                platforms.add(new Platform(2330, 300, 200, 30));
                platforms.add(new Platform(2930, 300, 320, 30));
                platforms.add(new Platform(3350, 300, 320, 30));
                platforms.add(new Platform(3990, 270, 70, 30));
                platforms.add(new Platform(4260, 270, 70, 30));
                platforms.add(new Platform(4605, 270, 70, 30));
                platforms.add(new Platform(4785, 270, 70, 30));
                platforms.add(new Platform(4965, 270, 70, 30));
                platforms.add(new Platform(5280, 300, 320, 30));
                platforms.add(new Platform(5700, 300, 320, 30));
                platforms.add(new Platform(6415, 300, 200, 30));
                platforms.add(new Platform(6730, 270, 70, 30));
                platforms.add(new Platform(6950, 270, 70, 30));
                platforms.add(new Platform(7130, 270, 70, 30));
                platforms.add(new Platform(7340, 270, 70, 30));
                platforms.add(new Platform(7680, 270, 70, 30));
                platforms.add(new Platform(7850, 270, 70, 30));
                platforms.add(new Platform(8225, 270, 70, 30));
                platforms.add(new Platform(8395, 300, 200, 30));
                platforms.add(new Platform(9000, 300, 320, 30));
                platforms.add(new Platform(9425, 300, 320, 30));
                platforms.add(new Platform(9765, 300, 320, 30));
                platforms.add(new Platform(10185, 300, 320, 30));
                platforms.add(new Platform(10800, 300, 320, 30));
                platforms.add(new Platform(11230, 300, 320, 30));
                platforms.add(new Platform(11600, 300, 320, 30));
                platforms.add(new Platform(12015, 300, 320, 30));
                platforms.add(new Platform(12495, 270, 605, 30));
                frogs.add(new Frog(5000));
                frogs.add(new Frog(3450));
                spiders.add(new Spider(2000));
                spiders.add(new Spider(6390));
                spiders.add(new Spider(9500));
                pinks.add(new Pink(500));
                pinks.add(new Pink(7870));
                pinks.add(new Pink(6660));
                pizzas.add(new Bonus(3000, 500, Bonus.PIZZA));
                pizzas.add(new Bonus(7000, 200, Bonus.PIZZA));
                pizzas.add(new Bonus(10000, 200, Bonus.PIZZA));
                shurikens.add(new Bonus(8000, 200, Bonus.SHURIKEN));
                lives.add(new Bonus(9000, 200, Bonus.LIFE));
                for(Frog frog: frogs){
                    addActor(frog);
                }
                for(Spider spider: spiders){
                    addActor(spider);
                }
                for(Pink pink: pinks){
                    addActor(pink);
                }
                break;
            case 2:
                deleteLevel();

                rightLine = 12000;
                downLine = 300;

                img = new Texture("images\\level2_1.png");
                img2 = new Texture("images\\level2_2.png");
                //level 2
                platforms.add(new Platform(180, 270, 70, 30));
                platforms.add(new Platform(340, 270, 70, 30));
                platforms.add(new Platform(560, 270, 70, 30));
                platforms.add(new Platform(825, 270, 70, 30));
                platforms.add(new Platform(990, 270, 140, 30));
                platforms.add(new Platform(1570, 300, 300, 30));
                platforms.add(new Platform(1990, 300, 300, 30));
                platforms.add(new Platform(2700, 300, 200, 30));
                platforms.add(new Platform(3000, 270, 540, 30));
                platforms.add(new Platform(3750, 270, 210, 30));
                platforms.add(new Platform(4225, 270, 350, 30));
                platforms.add(new Platform(4680, 300, 200, 30));
                platforms.add(new Platform(5675, 300, 200, 30));
                platforms.add(new Platform(6010, 270, 70, 30));
                platforms.add(new Platform(6195, 270, 70, 30));
                platforms.add(new Platform(6445, 270, 215, 30));
                platforms.add(new Platform(6905, 270, 380, 30));
                platforms.add(new Platform(7490, 270, 70, 30));
                platforms.add(new Platform(7655, 300, 200, 30));
                platforms.add(new Platform(8270, 300, 320, 30));
                platforms.add(new Platform(8690, 300, 320, 30));
                platforms.add(new Platform(9175, 270, 200, 30));
                platforms.add(new Platform(9550, 270, 200, 30));
                platforms.add(new Platform(9920, 270, 310, 30));
                platforms.add(new Platform(10640, 300, 310, 30));
                platforms.add(new Platform(11040, 300, 666, 30));
                platforms.add(new Platform(11825, 300, 320, 30));
                frogs.add(new Frog(1500));
                frogs.add(new Frog(3450));
                frogs.add(new Frog(4650));
                frogs.add(new Frog(7600));
                spiders.add(new Spider(2000));
                spiders.add(new Spider(6390));
                spiders.add(new Spider(9500));
                pinks.add(new Pink(500));
                pinks.add(new Pink(7870));
                pinks.add(new Pink(6660));
                pizzas.add(new Bonus(3000, 500, Bonus.PIZZA));
                pizzas.add(new Bonus(7000, 200, Bonus.PIZZA));
                pizzas.add(new Bonus(10000, 500, Bonus.PIZZA));
                for(Frog frog: frogs){
                    addActor(frog);
                }
                for(Spider spider: spiders){
                    addActor(spider);
                }
                for(Pink pink: pinks){
                    addActor(pink);
                }
                break;
            case 3:
                deleteLevel();

                rightLine = 12000;
                downLine = 300;

                img = new Texture("images\\level3_1.png");
                img2 = new Texture("images\\level3_2.png");
                //level 3
                platforms.add(new Platform(15, 270, 70, 30));
                platforms.add(new Platform(295, 270, 70, 30));
                platforms.add(new Platform(550, 270, 70, 30));
                platforms.add(new Platform(1055, 270, 170, 30));
                platforms.add(new Platform(1465, 300, 200, 30));
                platforms.add(new Platform(2110, 300, 320, 30));
                platforms.add(new Platform(2535, 300, 320, 30));
                platforms.add(new Platform(3165, 300, 320, 30));
                platforms.add(new Platform(3585, 300, 320, 30));
                platforms.add(new Platform(4105, 270, 170, 30));
                platforms.add(new Platform(4560, 270, 70, 30));
                platforms.add(new Platform(5315, 270, 550, 30));
                platforms.add(new Platform(6320, 270, 70, 30));
                platforms.add(new Platform(6500, 270, 170, 30));
                platforms.add(new Platform(7620, 300, 200, 30));
                platforms.add(new Platform(7985, 270, 70, 30));
                platforms.add(new Platform(8200, 270, 70, 30));
                platforms.add(new Platform(8405, 270, 70, 30));
                platforms.add(new Platform(9015, 270, 500, 30));
                platforms.add(new Platform(9610, 300, 200, 30));
                platforms.add(new Platform(10245, 300, 320, 30));
                platforms.add(new Platform(10880, 300, 320, 30));
                platforms.add(new Platform(11355, 300, 200, 30));
                platforms.add(new Platform(11715, 270, 70, 30));
                platforms.add(new Platform(12035, 270, 210, 30));
                frogs.add(new Frog(700));
                frogs.add(new Frog(3450));
                frogs.add(new Frog(7600));
                spiders.add(new Spider(1000));
                spiders.add(new Spider(6390));
                spiders.add(new Spider(9500));
                pinks.add(new Pink(500));
                pinks.add(new Pink(7870));
                pinks.add(new Pink(6660));
                pizzas.add(new Bonus(5200, 540, Bonus.PIZZA));
                pizzas.add(new Bonus(2000, 300, Bonus.PIZZA));
                pizzas.add(new Bonus(9666, 540, Bonus.PIZZA));
                shurikens.add(new Bonus(8000, 550, Bonus.SHURIKEN));
                lives.add(new Bonus(5000, 200, Bonus.LIFE));
                for(Frog frog: frogs){
                    addActor(frog);
                }
                for(Spider spider: spiders){
                    addActor(spider);
                }
                for(Pink pink: pinks){
                    addActor(pink);
                }
                break;
            case 4:
                deleteLevel();

                rightLine = 1790;
                downLine = 50;
                img = new Texture("images\\boss_level.png");
                platforms.add(new Platform(220, 310, 80, 30));
                platforms.add(new Platform(1315, 310, 240, 30));
                platforms.add(new Platform(625, 435, 575, 30));
                platforms.add(new Platform(585, 310, 30, 30));
                platforms.add(new Platform(1635, 380, 170, 30));
                platforms.add(new Platform(320, 570, 250, 30));
                boss = new Boss();
                addActor(boss);
                break;
        }
    }

    //отчиска старого левла
    public void deleteLevel(){
        playerSpawn();
        platforms.clear();
        pizzas.clear();

        for(Frog frog: frogs){
            removeActor(frog);
        }
        for(Pink pink: pinks){
            removeActor(pink);
        }
        for(Spider spider: spiders){
            removeActor(spider);
        }
        frogs.clear();
        pinks.clear();
        spiders.clear();
    }

    @Override
    public void act(float delta) {
        Audio.playGame();
        super.act(delta);

        setCamera();

        //проверки списков
        player.onPlatform = false;
        for(Platform plat: platforms) {
            if (player.getY() > plat.rect.getY() && plat.rect.overlaps(player.body) && !player.jump) {
                player.setY(plat.rect.getY() + plat.rect.getHeight() - 1);
                player.onPlatform = true;
            }
        }

        for(Bonus pizza: pizzas){
            if (player.body.overlaps(pizza.rect)){
                Audio.playBonus();
                pizza.alive = false;
                delete.add(pizza);
            }
        }

        for(Bonus shuriken: shurikens){
            if (player.body.overlaps(shuriken.rect) && shuriken.alive){
                Audio.playBonus();
                player.shurNum += 5;
                shuriken.alive = false;
                delete.add(shuriken);
            }
        }

        for(Bonus life: lives){
            if (player.body.overlaps(life.rect) && life.alive){
                Audio.playBonus();
                player.lifeNum += 1;
                life.alive = false;
                delete.add(life);
            }
        }

        for(Frog frog: frogs) {
            if (player.sword.overlaps(frog.body) && player.hit && frog.alive) {
                removeActor(frog);
                frog.alive = false;
            }
            if (player.shuriken.overlaps(frog.body) && (player.shurR || player.shurL) && frog.alive) {
                removeActor(frog);
                frog.alive = false;
                player.shurR = false;
                player.shurL = false;
            }
            if (player.body.overlaps(frog.body) && frog.alive) {
                playerDead();
            }
        }

        for(Spider spider: spiders) {

            if(spider.getX() - player.getX() < 150 && spider.getX() - player.getX() > -150){
                spider.up = false;
                spider.down = true;
            } else {
                spider.down = false;
                spider.up = true;
            }


            if (player.sword.overlaps(spider.body) && player.hit && spider.alive) {
                removeActor(spider);
                spider.alive = false;
            }
            if (player.shuriken.overlaps(spider.body) && (player.shurR || player.shurL) && spider.alive) {
                removeActor(spider);
                spider.alive = false;
                player.shurR = false;
                player.shurL = false;
            }
            if (player.body.overlaps(spider.body) && spider.alive) {
                playerDead();
            }
        }

        for(Pink pink: pinks) {
            if (player.sword.overlaps(pink.body) && player.hit && pink.alive) {
                playerDead();
                pink.alive = false;
            }
            if (player.shuriken.overlaps(pink.body) && (player.shurR || player.shurL) && pink.alive) {
                pink.alive = false;
                player.shurR = false;
                player.shurL = false;
            }
            if (player.body.overlaps(pink.body) && pink.alive) {
                playerDead();
                pink.alive = false;
            }
            if(pink.dead){
                removeActor(pink);
            }
        }

        for(Object object: delete){
            if(object instanceof Bonus){
                pizzas.remove(object);
            }
        }

        //провекра на конец левла
        if(pizzas.isEmpty() && level < 4){
            level += 1;
            setLevel(level);
        }

        if(level == 4 && !boss.alive){
            Audio.stopGame();
            gameScreen.ninjaCat.setGameOverScreen(true);
        }

        if(level == 4 && boss.alive) {
            if (boss.body.overlaps(player.body)) {
                playerDead();
            }
            if (boss.body.overlaps(player.sword) && boss.sit && player.hit) {
                damageBoss();
            }
            if (boss.body.overlaps(player.shuriken) && boss.sit && (player.shurR || player.shurL)) {
                damageBoss();
            }
        }
    }

    //движение камеры за игроком
    public void setCamera(){
        if(player.getX() > screenX+RIGHT && screenX + GameScreen.WIDTH - player.getWidth() < rightLine){
            screenX += 5;
            gameScreen.camera.translate(5, 0, 0);
        }
        if(player.getX() < screenX+LEFT && screenX >  leftLine){
            screenX -= 5;
            gameScreen.camera.translate(-5, 0, 0);
        }
        if(player.getY()+player.getHeight() > screenY+UP+100 && !player.fallen){
            screenY += 15;
            gameScreen.camera.translate(0, 15, 0);
        }
        if(player.getY() < screenY+DOWN && screenY > 0){
            screenY -= 20;
            gameScreen.camera.translate(0, -20, 0);
        }
        if(player.getX() < leftLine){
            player.setX(leftLine);
        }
        if(player.getX() > rightLine){
            player.setX(rightLine);
        }
    }

    public void playerDead(){
        if(player.lifeNum > 1) {
            Audio.playDead();
            player.lifeNum -= 1;
            playerReSpawn();
        } else{
            Audio.stopGame();
            player.lifeNum -= 1;
            gameScreen.ninjaCat.setGameOverScreen(false);
        }
    }

    public void playerSpawn(){
        screenX = 0;
        screenY = 0;
        gameScreen.newCamera();
        player.reSpawn();
    }

    public void playerReSpawn(){
//        screenX -= 1000;
//        screenY -= 1000;
        //if(player.getX() > 500) {
            System.out.println(getX());
            player.setX(player.getX() - 500);
            player.setY(100);
            player.setRect();
//        } else {
//            playerSpawn();
//        }
    }

    public void damageBoss(){
        if(boss.healthNum > 1){
            boss.healthNum -= 1;
            boss.spawn();
        } else {
            boss.alive = false;
            removeActor(boss);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX()-50, getY()-downLine);
        batch.draw(img2, 6031, getY()-downLine);
        for(Bonus pizza: pizzas){
            if(pizza.alive){
                pizza.draw(batch);
            }
        }

        for(Bonus shuriken: shurikens){
            if(shuriken.alive){
                shuriken.draw(batch);
            }
        }

        for(Bonus life: lives){
            if(life.alive){
                life.draw(batch);
            }
        }

        for(int i = 0; i < player.lifeNum; i++){
            batch.draw(imgLife, (i*LIFE_DRAW + LIFE_DRAW)+screenX, 550 + screenY);
        }
        for(int i = 0; i < player.shurNum; i++){
            batch.draw(imgShur, (SHUR_DRAW - i*60)+screenX , screenY, 50, 50);
        }

        if(level == 4){
            switch(boss.healthNum){
                case 3:
                    boss.health = boss.h3;
                    break;
                case 2:
                    boss.health = boss.h2;
                    break;
                case 1:
                    boss.health = boss.h1;
                    break;
            }
            batch.draw(boss.health, 450 + screenX, 550 + screenY);
        }

//        for(Platform plat: platforms){
//            plat.draw(batch);
//        }
        super.draw(batch, parentAlpha);
    }

    public void keyDown(int keycode){
        switch (keycode) {
            case Input.Keys.D:
                player.acts[0] = true;
                break;
            case Input.Keys.RIGHT:
                player.acts[0] = true;
                break;
            case Input.Keys.A:
                player.acts[1] = true;
                break;
            case Input.Keys.LEFT:
                player.acts[1] = true;
                break;
            case Input.Keys.W:
                player.acts[2] = true;
                break;
            case Input.Keys.SPACE:
                player.acts[2] = true;
                break;
            case Input.Keys.UP:
                player.acts[2] = true;
                break;
            case Input.Keys.Q:
                player.acts[3] = true;
                break;
            case Input.Keys.E:
                player.acts[4] = true;
                break;
        }
    }

    public void keyUp(int keycode){
        switch (keycode) {
            case Input.Keys.D:
                player.acts[0] = false;
                break;
            case Input.Keys.RIGHT:
                player.acts[0] = false;
                break;
            case Input.Keys.A:
                player.acts[1] = false;
                break;
            case Input.Keys.LEFT:
                player.acts[1] = false;
                break;
            case Input.Keys.W:
                player.acts[2] = false;
                break;
            case Input.Keys.SPACE:
                player.acts[2] = false;
                break;
            case Input.Keys.UP:
                player.acts[2] = false;
                break;
            case Input.Keys.Q:
                player.acts[3] = false;
                break;
            case Input.Keys.E:
                player.acts[4] = false;
                break;
            case Input.Keys.S:
                player.setY(player.getY() - 30);
                player.setRect();
                break;
            case Input.Keys.DOWN:
                player.setY(player.getY()-30);
                player.setRect();
                break;
        }
    }
}
