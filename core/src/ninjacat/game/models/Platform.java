package ninjacat.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;

public class Platform {

    Rectangle rect;
    Texture img;

    public Platform(float x, float y, float w, float h){
        rect = new Rectangle(x, y, w, h);
        img = new Texture("images\\h3.png");
    }

    public void draw(Batch batch){
        batch.draw(img, rect.x, rect.y, rect.width, rect.height);
    }
}
