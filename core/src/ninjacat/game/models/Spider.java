package ninjacat.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ninjacat.game.util.Audio;

public class Spider extends Actor {

    boolean down;
    boolean up;
    boolean alive;
    float speed = 0;

    Rectangle body;

    Texture img;

    public Spider(float x){
        up = false;
        down = false;
        alive = true;
        setBounds(x, 600, 100, 750);
        body  = new Rectangle(getX(), getY(), getWidth(), 100);
        img = new Texture("images\\spider.png");
    }

    public void setRect(){
        body.set(getX(), getY(), getWidth(), 100);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        down();
        up();
    }

    public void down(){
        if(down){
            speed += 0.5f;
            setY(getY() - speed);
            setRect();
        }
        if(getY() < Level.FLOOR){
            setY(Level.FLOOR);
            setRect();
        }
        if(getY() == Level.FLOOR){
            down = false;
            speed = 5;
        }
    }

    public void up(){
        if(up){
            setY(getY() + speed);
            setRect();
        }
        if(getY() > 500){
            setY(500);
            setRect();
        }
        if(getY() == 500){
            up = false;
            speed = 0;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(img, getX(), getY());
    }
}
