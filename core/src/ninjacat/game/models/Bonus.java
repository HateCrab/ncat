package ninjacat.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;

public class Bonus {
    public static final int PIZZA = 1;
    public static final int SHURIKEN = 2;
    public static final int LIFE = 3;

    public Rectangle rect;
    public int state;
    Texture img;

    public boolean alive;

    public Bonus(float x, float y, int state){
        this.state = state;
        alive = true;
        createBonus(x, y);
    }

    public void createBonus(float x, float y){
        switch (state){
            case PIZZA:
                img = new Texture("images\\pizza.png");
                rect = new Rectangle(x, y, 110, 80);
                break;
            case SHURIKEN:
                img = new Texture("images\\shuriken.png");
                rect = new Rectangle(x, y, 60, 60);
                break;
            case LIFE:
                img = new Texture("images\\life.png");
                rect = new Rectangle(x, y, 50, 50);
                break;
        }
    }

    public void draw(Batch batch){
        batch.draw(img, rect.x, rect.y, rect.width, rect.height);
    }
}
