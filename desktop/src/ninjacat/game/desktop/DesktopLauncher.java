package ninjacat.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ninjacat.game.NinjaCat;
import ninjacat.game.screens.AbstractScreen;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.width = AbstractScreen.WIDTH;
        config.height = AbstractScreen.HEIGHT;
		new LwjglApplication(new NinjaCat(), config);
	}
}
